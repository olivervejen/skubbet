﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skubbet.Abstracts {
    abstract class GameObject {
        protected int tileSize;
        public GameObject(int tileSize) 
        {
            this.tileSize = tileSize;
        }

        /// <summary>
        /// Renders every room and entity using GDI.
        /// </summary>
        /// <param name="g">An instance of the GDI drawing surface</param>
        public virtual void Render(Graphics g) {
        }
        public virtual void Tick(float delta) {
        }
    }
}
