﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skubbet.Abstracts {
    public abstract class GameState {
        public GameState(Window w) {
        }

        public abstract void Render(Graphics g);

        public abstract void Tick(float delta);
    }
}
