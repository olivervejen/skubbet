﻿using Skubbet.Entities;
using Skubbet.Entities.LivingEntities;
using Skubbet.Entities.NonLivingEntities;
using Skubbet.Game;
using Skubbet.World;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Skubbet.Abstracts
{
    abstract class Level : GameObject
    {
        protected List<Room> rooms = new List<Room>();
        protected List<LivingEntity> npcs = new List<LivingEntity>();
        protected List<NonLivingEntity> nonLivings = new List<NonLivingEntity>();
        protected List<Tile> overlays = new List<Tile>();
        protected float overlayTicks = 0;
        private Player p;

        /// <summary>
        /// A level is a collection of rooms and entities.
        /// </summary>
        /// <param name="tileSize">How big one tile is in pixels</param>
        /// <param name="x">where on the x axix of the window the level is in pixels</param>
        /// <param name="y">where on the y axix of the window the level is in pixels</param>
        public Level(int tileSize, int x, int y, Player p) : base(tileSize)
        {
            this.p = p;
        }

        public override void Render(Graphics g) 
        {
            
            foreach (Room room in rooms)
            {
                if(room == GetRoomAt(p.x, p.y))
                {
                    room.Render(g); 
                    foreach (Entity nl in EntitisInRoom(room)) {
                        nl.Render(g);
                    }
                    foreach (Tile t in overlays)
                    {
                        g.FillRectangle(t.brush, t.x*tileSize, t.y*tileSize, tileSize, tileSize);
                    }
                } else
                {
                    room.RenderUnoccupied(g, @"Images\Roof.png");
                }

            }
            
        }
        
        public void AddOverlay(Tile t)
        {
            overlays.Add(t);
        }

        public override void Tick(float delta)
        {
            if (overlays.Any())
            {
                overlayTicks += 1 * delta;
            }
            Window.debug.Log("" + overlayTicks);
            foreach (Room room in rooms)
            {
                room.Tick(delta);
            }
            if(overlayTicks > 200)
            {
                overlays.Clear();
                overlayTicks = 0;
            }
        }
        /// <summary>
        /// Informs if the tile above a given point is collidable
        /// </summary>
        /// <param name="x">x of the point</param>
        /// <param name="y">y of the point</param>
        /// <returns>returns true if the tile above the point is collidable and false if not</returns>
        public bool GetColAt(int x, int y)
        {
            if (GetRoomAt(x, y) != null)
            {
                if(GetRoomAt(x, y).getTileAt(x, y).col || GetEntityAt(x, y) != null || (p.x == x && p.y == y)) {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Updates every entity in the level.
        /// </summary>
        public void Update(Room room) {
            List<LivingEntity> temp = npcs.ToList();
            foreach (LivingEntity npc in temp)
            {
                if(npcs.Where(x => x == npc).Any())
                {
                    npcs.First(x => x == npc).Behavior();
                }
            }
        }
        /// <summary>
        /// Gets the room that is under a given point
        /// </summary>
        /// <param name="x">x of the point</param>
        /// <param name="y">y of the point</param>
        /// <returns>The room under the point</returns>
        public Room GetRoomAt(int x, int y)
        {
            if(rooms.Where(r => r.x - 1 < x && r.x + r.GetWidth() > x && r.y - 1 < y && r.y + r.GetHeight() > y).Any())
            {
                return rooms.Last(r => r.x - 1 < x && r.x + r.GetWidth() > x && r.y - 1 < y && r.y + r.GetHeight() > y);
            }
            return null;
        }
        /// <summary>
        /// Gets the index of a given room in this levels rooms
        /// </summary>
        /// <param name="r">the room to check</param>
        /// <returns>The index of the room. If there is no room then return "No room"</returns>
        public object GetRoomIndex(Room r)
        {
            if(r != null)
            {
                return rooms.IndexOf(r);
            }
            return "No room";
        }
        
        /// <summary>
        /// removes a given entity from the level, if it exists in the level
        /// </summary>
        /// <param name="e">The entity to be removed</param>
        public void RemoveEntity(Entity e)
        {
            if (e != null)
            {
                if (nonLivings.Where(x => x == e).Any())
                {
                   nonLivings.Remove(nonLivings.First(x => x == e));
                }
                if (npcs.Where(x => x == e).Any())
                {
                    npcs.Remove(npcs.First(x => x == e));
                }
            }
        }
        /// <summary>
        /// adds an entity to the level
        /// </summary>
        /// <param name="e">the entity to be added</param>
        public void AddEntity(Entity e) {
            if (e != null) {
                if (typeof(NonLivingEntity).IsInstanceOfType(e)) {
                    nonLivings.Add((NonLivingEntity)e);
                }
                if (typeof(LivingEntity).IsInstanceOfType(e)) {
                    npcs.Add((LivingEntity)e);
                }
            }
        }

        /// <summary>
        /// Gets all entities in a given room
        /// </summary>
        /// <param name="room">the room to check</param>
        /// <returns>a list of entities in room</returns>
        public List<Entity> EntitisInRoom(Room room)
        {
            List<Entity> a = new List<Entity>();
            a.AddRange(npcs);
            a.AddRange(nonLivings);
            if (room != null)
            {
                if (a.Where(x => x.GetRoom(this) == room).Any())
                {
                    return a.Where(x => x.GetRoom(this) == room).ToList();
                }
            }
            return new List<Entity>();
        }
        /// <summary>
        /// gets an entity at a certain tile
        /// </summary>
        /// <param name="x">x of the til</param>
        /// <param name="y">y of the tile</param>
        /// <returns>the entity of the tile, if there is any. else null</returns>
        public Entity GetEntityAt(int x, int y) {
            List<Entity> a = new List<Entity>();
            a.AddRange(npcs);
            a.AddRange(nonLivings);
            if(a.Where(e => e.x == x && e.y == y).Any()) {
                return a.First(e => e.x == x && e.y == y);
            }
            return null;
        }


    }
}
