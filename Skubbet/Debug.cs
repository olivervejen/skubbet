﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skubbet {
    public partial class Debug : Form {
        public Debug() {
            InitializeComponent();
            this.Location = new Point(1000, 0);
            this.StartPosition = FormStartPosition.Manual;

        }

        public void Log(string log) {
            listBox1.TopIndex = listBox1.Items.Count-1;
            listBox1.Items.Add(log);
        }
    }
}
