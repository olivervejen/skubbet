﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Skubbet.Abstracts;
using Skubbet.World;
using Skubbet.Entities.NonLivingEntities.Usable;

namespace Skubbet.Entities
{
    abstract class Entity : GameObject
    {
        protected int width;
        protected int height;
        protected Image sprite;
        protected Level lvl;
        public int x;
        public int y;
        /// <summary>
        /// An interactable object in the gameworld
        /// </summary>
        /// <param name="width">entitys height</param>
        /// <param name="height">entitys width</param>
        /// <param name="spriteDest">path to an image to use as a sprite</param>
        /// <param name="x">the x position of the entity</param>
        /// <param name="y">the y position of the entity</param>
        /// <param name="tileSize">the size of a single tile</param>
        public Entity(int width, int height, string spriteDest, int x, int y, int tileSize) : base(tileSize)
        {
            this.width = width;
            this.height = height;
            sprite = Image.FromFile(spriteDest);
            this.x = x;
            this.y = y;
        }
        public override void Render(Graphics g)
        {
            g.DrawImage(sprite, x * tileSize, y * tileSize);
        }
        public override void Tick(float delta)
        {
        }
        public abstract void TakeDamage(Weapon w);
        public Room GetRoom(Level lvl) {
            return lvl.GetRoomAt(x, y);
        }
    }
}
