﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.NonLivingEntities.Usable;
using Skubbet.Game;

namespace Skubbet.Entities.LivingEntities
{
    class AggressiveEnemy : LivingEntity
    {
        public AggressiveEnemy(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            health = 50;
            sprite = Image.FromFile(@"Images/Enemy2.png");
            deadSprite = Image.FromFile(@"Images/Error.png");
            weapon = new EnemyClaws(0, 0, 1, lvl);
        }

        /// <summary>
        /// Moves randomly.
        /// </summary>
        public override void Behavior()
        {
            int randx = Window.rnd.Next(0, 2);
            int randy = Window.rnd.Next(0, 2);
            int randA = Window.rnd.Next(0, 2);

            if (OverWorldGameState.p.y < y && OverWorldGameState.p.x == x && OverWorldGameState.p.y >= y - 1)
            {
                Window.debug.Log("AttackUp");
                sprite = Image.FromFile(@"Images/Enemy2Up.png");
                weapon.AttackUp(this);
            }
            if (OverWorldGameState.p.y > y && OverWorldGameState.p.x == x && OverWorldGameState.p.y <= y + 1)
            {
                Window.debug.Log("AttackDown");
                sprite = Image.FromFile(@"Images/Enemy2.png");
                weapon.AttackDown(this);
            }
            if (OverWorldGameState.p.x < x && OverWorldGameState.p.y == y && OverWorldGameState.p.x >= x - 1)
            {
                Window.debug.Log("AttackLeft");
                sprite = Image.FromFile(@"Images/Enemy2Left.png");
                weapon.AttackLeft(this);
            }
            if (OverWorldGameState.p.x > x && OverWorldGameState.p.y == y && OverWorldGameState.p.x <= x + 1)
            {
                Window.debug.Log("AttackRight");
                sprite = Image.FromFile(@"Images/Enemy2Right.png");
                weapon.AttackRight(this);
            }

            if (OverWorldGameState.p.y < y && randx == 1 && OverWorldGameState.p.y < y + 7 && OverWorldGameState.p.x < x + 7)
            {
                MoveUp();
                sprite = Image.FromFile(@"Images/Enemy2Up.png");
            }
            if (OverWorldGameState.p.y > y && randx == 1 && OverWorldGameState.p.y < y + 7 && OverWorldGameState.p.x < x + 7)
            {
                MoveDown();
                sprite = Image.FromFile(@"Images/Enemy2.png");
            }
            if (OverWorldGameState.p.x < x && randy == 1 && OverWorldGameState.p.y < y + 7 && OverWorldGameState.p.x < x + 7)
            {
                MoveLeft();
                sprite = Image.FromFile(@"Images/Enemy2Left.png");
            }
            if (OverWorldGameState.p.x > x && randy == 1 && OverWorldGameState.p.y < y + 7 && OverWorldGameState.p.x < x + 7)
            {
                MoveRight();
                sprite = Image.FromFile(@"Images/Enemy2Right.png");
            }
        }

    }
}