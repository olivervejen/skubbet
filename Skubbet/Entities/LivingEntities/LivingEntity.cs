﻿using Skubbet.Abstracts;
using Skubbet.Entities.NonLivingEntities.Usable;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skubbet.Entities.LivingEntities
{
    abstract class LivingEntity : Entity
    {
        protected Image deadSprite;
        public Weapon weapon;
        public int health;
        protected int fullHealth;
        public LivingEntity(int x, int y, int tileSize, Level lvl) : base(1, 1, @"Images/Error.png", x, y, tileSize)
        {
            this.lvl = lvl;
            deadSprite = Image.FromFile(@"Images/Error.png");
            fullHealth = health;
        }

        /// <summary>
        /// The behavior of a LivingEntity. Handles movement and AI
        /// </summary>
        /// <param name="lvl">A level for collision and AI purposes</param>
        public abstract void Behavior();
        /// <summary>
        /// sets what level the living entity is part of.
        /// </summary>
        /// <param name="lvl"></param>
        public void SetLevel(Level lvl) {
            this.lvl = lvl;
        }

        public override void Render(Graphics g)
        {
            base.Render(g);
            Rectangle rect = new Rectangle((x * tileSize) - tileSize / 4, (y - 1) * tileSize, tileSize + tileSize / 2, tileSize);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            g.DrawString("" + health, new Font("Arial", 10), Brushes.Black, rect, stringFormat);
        }

        /// <summary>
        /// Moves the LivingEntity up if it does not collide
        /// </summary>
        /// <param name="lvl">What level to check collision against</param>
        public void MoveUp()
        {

            if (!lvl.GetColAt(x, y - 1))
            {
                this.y -= 1;
            }
        }


        /// <summary>
        /// Moves the LivingEntity down if it does not collide
        /// </summary>
        /// <param name="lvl">What level to check collision against</param>
        public void MoveDown()
        {
            if (!lvl.GetColAt(x, y + 1))
            {
                this.y += 1;
            }
        }
        /// <summary>
        /// Moves the LivingEntity left if it does not collide
        /// </summary>
        /// <param name="lvl">What level to check collision against</param>
        public void MoveLeft()
        {
            if (!lvl.GetColAt(x - 1, y))
            {
                this.x -= 1;
            }
        }
        /// <summary>
        /// Moves the LivingEntity right if it does not collide
        /// </summary>
        /// <param name="lvl">What level to check collision against</param>
        public void MoveRight()
        {
            if (!lvl.GetColAt(x + 1, y))
            {
                this.x += 1;
            }
        }
        /// <summary>
        /// Adds a weapon to the living entity
        /// </summary>
        /// <param name="w">the weapon to add</param>
        public virtual void AddWeapon(Weapon w)
        {
            weapon = w;
        }
        /// <summary>
        /// kills the living entity. removes it from the level
        /// </summary>
        public void Die()
        {
            lvl.RemoveEntity(this);
        }
        /// <summary>
        /// health is decreased with the weapons damage, when it reaches 0 the entity will <see cref="Die"/>
        /// </summary>
        /// <param name="w">the weapon that the entity is damaged with</param>
        public override void TakeDamage(Weapon w)
        {
            health -= w.damage;
            if (health <= 0)
            {
                Die();
            }
        }
    }
}
