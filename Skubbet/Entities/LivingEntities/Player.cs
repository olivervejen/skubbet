﻿using Skubbet.Abstracts;
using Skubbet.Entities.NonLivingEntities.Usable;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skubbet.Entities.LivingEntities
{
    class Player : LivingEntity
    {
        public Player(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            sprite = Image.FromFile(@"Images\Player2.png");
            deadSprite = Image.FromFile(@"Images\Player2.png");
            health = 300;
            fullHealth = 300;
        }
        /// <summary>
        /// Moves the player by pressed keys
        /// </summary>
        public override void Behavior()
        {
            if (Window.AnyKeyReleased())
            {

                if (Window.IsKeyReleased(Keys.W))
                {
                    if (lvl.GetEntityAt(x, y - 1) != null)
                    {
                        if (typeof(UsableEntity).IsAssignableFrom(lvl.GetEntityAt(x, y - 1).GetType()))
                        {
                            UsableEntity u = (UsableEntity)lvl.GetEntityAt(x, y - 1);
                            u.Pickup(this);
                        }
                    }
                    MoveUp();

                }
                if (Window.IsKeyReleased(Keys.S))
                {
                    if (lvl.GetEntityAt(x, y + 1) != null)
                    {
                        if (typeof(UsableEntity).IsAssignableFrom(lvl.GetEntityAt(x, y + 1).GetType()))
                        {
                            UsableEntity u = (UsableEntity)lvl.GetEntityAt(x, y + 1);
                            u.Pickup(this);
                        }
                    }
                    MoveDown();
                }
                if (Window.IsKeyReleased(Keys.A))
                {
                    if (lvl.GetEntityAt(x - 1, y) != null)
                    {
                        if (typeof(UsableEntity).IsAssignableFrom(lvl.GetEntityAt(x - 1, y).GetType()))
                        {
                            UsableEntity u = (UsableEntity)lvl.GetEntityAt(x - 1, y);
                            u.Pickup(this);
                        }
                    }
                    MoveLeft();
                }
                if (Window.IsKeyReleased(Keys.D))
                {
                    if (lvl.GetEntityAt(x + 1, y) != null)
                    {
                        if (typeof(UsableEntity).IsAssignableFrom(lvl.GetEntityAt(x + 1, y).GetType()))
                        {
                            UsableEntity u = (UsableEntity)lvl.GetEntityAt(x + 1, y);
                            u.Pickup(this);
                        }
                    }
                    MoveRight();
                }
                if (weapon != null)
                {
                    if (Window.IsKeyReleased(Keys.Up))
                    {
                        weapon.AttackUp(this);
                    }
                    if (Window.IsKeyReleased(Keys.Down))
                    {
                        weapon.AttackDown(this);
                    }
                    if (Window.IsKeyReleased(Keys.Left))
                    {
                        weapon.AttackLeft(this);
                    }
                    if (Window.IsKeyReleased(Keys.Right))
                    {
                        weapon.AttackRight(this);
                    }
                    if (Window.IsKeyReleased(System.Windows.Forms.Keys.Q)) {
                        weapon.Drop(x, y);
                        weapon = null;
                    }
                }
                lvl.Update(lvl.GetRoomAt(x, y));
            }
        }
        public override void TakeDamage(Weapon w)
        {
            health -= w.damage;
        }
    }
}
