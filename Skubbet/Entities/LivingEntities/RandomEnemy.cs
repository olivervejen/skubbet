﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.NonLivingEntities.Usable;

namespace Skubbet.Entities.LivingEntities {
    class RandomEnemy : LivingEntity {
        public RandomEnemy(int x, int y, int tileSize, Level lvl) :base(x, y, tileSize, lvl) {
            health = 100;
            sprite = Image.FromFile(@"Images/Enemy1.png");
            deadSprite = Image.FromFile(@"Images/Error.png");
            weapon = new BoxingGlove(0, 0, 1, lvl);
        }
        
        /// <summary>
        /// Moves randomly.
        /// </summary>
        public override void Behavior() {
            int randx = Window.rnd.Next(0, 10);
            int randy = Window.rnd.Next(0, 10);
            int randA = Window.rnd.Next(0, 8);

            if (randy == 1)
            {
                MoveUp();
            }
            if (randy == 2)
            {
                MoveDown();
            }
            if (randx == 1)
            {
                MoveLeft();
            }
            if (randx == 2)
            {
                MoveRight();
            }
            if(randA == 1)
            {
                Window.debug.Log("AttackUp");
                weapon.AttackUp(this);
            }
            if (randA == 2)
            {
                Window.debug.Log("AttackDown");
                weapon.AttackDown(this);
            }
            if (randA == 3)
            {
                Window.debug.Log("AttackLeft");
                weapon.AttackLeft(this);
            }
            if (randA == 4)
            {
                Window.debug.Log("AttackRight");
                weapon.AttackRight(this);
            }
        }
        
    }
}
