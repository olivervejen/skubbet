﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.NonLivingEntities.Usable;
using Skubbet.Game;

namespace Skubbet.Entities.LivingEntities
{
    class SniperEnemy : LivingEntity
    {
        public SniperEnemy(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            health = 90;
            sprite = Image.FromFile(@"Images/Enemy3Up.png");
            deadSprite = Image.FromFile(@"Images/Error.png");
            weapon = new EnemySniperLaserBeam(0, 0, 1, lvl);
        }

        /// <summary>
        /// Moves randomly.
        /// </summary>
        public override void Behavior()
        {
            int randx = Window.rnd.Next(0, 12);
            int randy = Window.rnd.Next(0, 12);
            int randA = Window.rnd.Next(0, 2);

            if (OverWorldGameState.p.y < y && OverWorldGameState.p.x == x && OverWorldGameState.p.y >= y - 8)
            {
                Window.debug.Log("AttackUp");
                sprite = Image.FromFile(@"Images/Enemy3Up.png");
                weapon.AttackUp(this);
            }
            if (OverWorldGameState.p.y > y && OverWorldGameState.p.x == x && OverWorldGameState.p.y <= y + 8)
            {
                Window.debug.Log("AttackDown");
                sprite = Image.FromFile(@"Images/Enemy3Down.png");
                weapon.AttackDown(this);
            }
            if (OverWorldGameState.p.x < x && OverWorldGameState.p.y == y && OverWorldGameState.p.x >= x - 8)
            {
                Window.debug.Log("AttackLeft");
                sprite = Image.FromFile(@"Images/Enemy3Left.png");
                weapon.AttackLeft(this);
            }
            if (OverWorldGameState.p.x > x && OverWorldGameState.p.y == y && OverWorldGameState.p.x <= x + 8)
            {
                Window.debug.Log("AttackRight");
                sprite = Image.FromFile(@"Images/Enemy3Right.png");
                weapon.AttackRight(this);
            }

            if (OverWorldGameState.p.y < y && randx == 1 && OverWorldGameState.p.y < y + 12 && OverWorldGameState.p.x < x + 12)
            {
                MoveUp();
                sprite = Image.FromFile(@"Images/Enemy3Up.png");
            }
            if (OverWorldGameState.p.y > y && randx == 1 && OverWorldGameState.p.y < y + 12 && OverWorldGameState.p.x < x + 12)
            {
                MoveDown();
                sprite = Image.FromFile(@"Images/Enemy3Down.png");
            }
            if (OverWorldGameState.p.x < x && randy == 1 && OverWorldGameState.p.y < y + 12 && OverWorldGameState.p.x < x + 12)
            {
                MoveLeft();
                sprite = Image.FromFile(@"Images/Enemy3Left.png");
            }
            if (OverWorldGameState.p.x > x && randy == 1 && OverWorldGameState.p.y < y + 12 && OverWorldGameState.p.x < x + 12)
            {
                MoveRight();
                sprite = Image.FromFile(@"Images/Enemy3Right.png");
            }
        }

    }
}