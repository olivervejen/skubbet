﻿using Skubbet.Abstracts;
using Skubbet.Entities.NonLivingEntities.Usable;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skubbet.Entities.NonLivingEntities {
    class NonLivingEntity : Entity {
        protected int durability = 10;
        protected int fullDurability = 10;
        public string name { get; set; }
        /// <summary>
        /// An entity that does not have any kind of behavior other than sitting and being able to be destroyed
        /// </summary>
        /// <param name="width">entitys height</param>
        /// <param name="height">entitys width</param>
        /// <param name="spriteDest">path to an image to use as a sprite</param>
        /// <param name="x">the x position of the entity</param>
        /// <param name="y">the y position of the entity</param>
        /// <param name="tileSize">the size of a single tile</param>
        /// <param name="durability">The durability of the entity</param>
        /// <param name="name">The name of the entity, to identify it later</param>
        /// <param name="lvl">the level the entity exists in</param>
        public NonLivingEntity(int width, int height, string spriteDest, int x, int y, int tileSize, int durability, string name, Level lvl) : base(width, height, spriteDest, x, y, tileSize) {
            this.durability = durability;
            this.fullDurability = durability;
            this.name = name;
            this.lvl = lvl;
        }

       /// <summary>
       /// destroys the nonliving entity
       /// </summary>
        public void Break() {
            durability = 0;
            lvl.RemoveEntity(this);
        }

        public override void Render(Graphics g) {
            if (durability > 0) {
                base.Render(g);
            }
        }
        /// <summary>
        /// durability is decreased with the weapons damage, when it reaches 0 the entity will <see cref="Break"/>
        /// </summary>
        /// <param name="w">the weapon that the entity is damaged with</param>
        public override void TakeDamage(Weapon w) {
            durability -= w.damage;
            if(durability <= 0) {
                Break();
            }
        }
    }
}
