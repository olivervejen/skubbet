﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class Bomb : Weapon
    {
        public Bomb(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Giant Bomb";
            sprite = Image.FromFile(@"Images/Bomb.png");
            damage = 10;
            description = "A gaint bomb toy, but its deadly! it does " + damage + " damage";
            pattern = new List<Point> { new Point(0, -2),  new Point(0, -3), new Point(0, -4), new Point(-1, -3), new Point(1, -3) };
        }
    }
}
