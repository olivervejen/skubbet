﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class Bow : Weapon
    {
        public Bow(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Bow and arrow";
            sprite = Image.FromFile(@"Images/BowAndArrow.png");
            damage = 6;
            description = "Its like a bow and have some arrows! it does " + damage + " damage";
            pattern = new List<Point>();
            for (int i = 1; i < 4; i++)
            {
                pattern.Add(new Point(0, -i));
            }
        }
    }
}
