﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class BoxingGlove : Weapon
    {
        public BoxingGlove(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "boxing glove";
            sprite = Image.FromFile(@"Images/NonLiving.png");
            damage = 30;
            description = "this is a boxing glove that does " +  damage + " damage";
            pattern = new List<Point> { new Point(-1, -1), new Point(0, -1), new Point(1, -1), new Point(0, -2) };
        }
    }
}
