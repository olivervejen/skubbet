﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class Branch : Weapon
    {
        public Branch(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Branch Stick";
            sprite = Image.FromFile(@"Images/Branch.png");
            damage = 1;
            description = "Worst weapon you can find! it does deadly " + damage + " damage!";
            pattern = new List<Point> { new Point(0, -1) };
        }
    }
}
