﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class DualHammers : Weapon
    {
        public DualHammers(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "DualHammers";
            sprite = Image.FromFile(@"Images/DualHammers.png");
            damage = 12;
            description = "Two Hammers at once! It does " + damage + " damage";
            pattern = new List<Point> { new Point(-1, -1), new Point(0, -1), new Point(1, -1), new Point(0, -2) };
        }
    }
}
