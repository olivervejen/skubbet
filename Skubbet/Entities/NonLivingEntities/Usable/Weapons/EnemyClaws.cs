﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class EnemyClaws : Weapon
    {
        public EnemyClaws(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Enemy Claws";
            sprite = Image.FromFile(@"Images/Error.png");
            damage = 10;
            description = "This is the enemy weapon. It does " + damage + " damage";
            pattern = new List<Point> { new Point(0, -1) };
        }
    }
}