﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class EnemySniperLaserBeam : Weapon
    {
        public EnemySniperLaserBeam(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Laser Beam";
            sprite = Image.FromFile(@"Images/NonLiving.png");
            damage = 20;
            description = "This is a murderbeam that kills unwary players by dealing " + damage + " damage";
            pattern = new List<Point> { new Point(0, -1), new Point(0, -2), new Point(0, -3), new Point(0, -4), new Point(0, -5), new Point(0, -6), new Point(0, -7), new Point(0, -8) };
        }
    }
}