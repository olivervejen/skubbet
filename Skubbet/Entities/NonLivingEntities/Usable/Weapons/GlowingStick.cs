﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using Skubbet.Entities.LivingEntities;
using System.Drawing;
using Skubbet.Game;
using Skubbet.World;


namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class GlowingStick : Weapon
    {
        public GlowingStick(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Glowing Sticks";
            sprite = Image.FromFile(@"Images/GlowingStick.png");
            damage = 13;
            description = "this is glowing sticks! Hot on the tip and it does " + damage + " damage!";
            pattern = new List<Point> { new Point(-1, -1), new Point(0, -1), new Point(1, -1), new Point(0, -2) };
        }
    }
}
