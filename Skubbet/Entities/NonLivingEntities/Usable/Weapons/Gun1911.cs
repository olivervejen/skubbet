﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class Gun1911 : Weapon
    {
        public Gun1911(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            description = "this is a alien version of Colt M1911 that does 2 damage!";
            name = "Colt M1911";
            sprite = Image.FromFile(@"Images/1911.png");
            damage = 2;
            pattern = new List<Point>();
            for (int i = 1; i < 6; i++)
            {
                pattern.Add(new Point(0, -i));
            }
        }
    }
}
