﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class HeavyHammer : Weapon
    {
        public HeavyHammer(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Heavy Hammer";
            sprite = Image.FromFile(@"Images/HeavyHammer.png");
            damage = 13;
            description = "ugh, that a weird heavy hammer, but usefull in close combat, its does " + damage + " damage";
            pattern = new List<Point> { new Point(-1, -1), new Point(0, -1), new Point(1, -1), new Point(1, 0), new Point(-1, 0) };
        }
    }
}
