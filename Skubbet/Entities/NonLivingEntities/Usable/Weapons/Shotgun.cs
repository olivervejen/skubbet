﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class Shotgun : Weapon
    {
        public Shotgun(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Shotgun";
            sprite = Image.FromFile(@"Images/Shotgun.png");
            damage = 14;
            description = "Deadly alien shotgun, equals to a paintball gun on earth - Does " + damage + " damage";
            pattern = new List<Point> { new Point(0, -1), new Point(0, -2), new Point(0, -3), new Point(1, -3),
                new Point(1, -2), new Point(-2, -3), new Point(2, -3), new Point(-1, -3), new Point(-1, -2) };
        }
    }
}
