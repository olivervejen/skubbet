﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using System.Drawing;
using Skubbet.Entities.LivingEntities;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    class SniperRifle : Weapon
    {
        public SniperRifle(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            name = "Sniper Rifle";
            sprite = Image.FromFile(@"Images\Rifle.png");
            damage = 20;
            description = "The Deadliest gun in the game! But limited ammo! The damage is on " + damage;
            pattern = new List<Point>();
            for (int i = 1; i < 10; i++)
            {
                pattern.Add(new Point(0, -i));
            }
        }
    }
}