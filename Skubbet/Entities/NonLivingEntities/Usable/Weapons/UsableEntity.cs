﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using Skubbet.Entities.LivingEntities;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    abstract class UsableEntity : NonLivingEntity
    {
        protected string description = "This item is missing a description";
        /// <summary>
        /// An entity that <see cref="LivingEntity"/> can pick up and use.
        /// </summary>
        /// <param name="x">the x position of the entity</param>
        /// <param name="y">the y position of the entity</param>
        /// <param name="tileSize">the size of a single tile</param>
        /// <param name="lvl">the level the entity exists in</param>
        public UsableEntity(int x, int y, int tileSize, Level lvl) : base(1, 1, @"Images/Error.png", x, y, tileSize, 10, "UsableEntity", lvl)
        {
            name = "This item is missing a description";  
        }
        /// <summary>
        /// Is called when it is picked up by a <see cref="LivingEntity"/>
        /// </summary>
        /// <param name="e">the <see cref="LivingEntity"/> that picks it up</param>
        public virtual void Pickup(LivingEntity e) {
            this.Break();
        }
        /// <summary>
        /// drops the entity at a point that does not collide
        /// </summary>
        /// <param name="x">the <see cref="LivingEntity"/> that drops its x</param>
        /// <param name="y">the <see cref="LivingEntity"/> that drops its y</param>
        public virtual void Drop(int x, int y)
        {
            if (lvl.GetColAt(x, y - 1))
            {
                if (lvl.GetColAt(x, y + 1))
                {
                    if (lvl.GetColAt(x - 1, y))
                    {
                        this.x = x + 1;
                        this.y = y;

                    }
                    else
                    {
                        this.x = x - 1;
                        this.y = y;
                    }
                }
                else
                {
                    this.x = x;
                    this.y = y + 1;
                }
            }
            else
            {
                this.x = x;
                this.y = y - 1;
            }
            
            
            this.durability = fullDurability;
            lvl.AddEntity(this);
        }
    }
}
