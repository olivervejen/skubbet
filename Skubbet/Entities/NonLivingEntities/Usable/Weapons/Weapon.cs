﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skubbet.Abstracts;
using Skubbet.Entities.LivingEntities;
using System.Drawing;
using Skubbet.Game;
using Skubbet.World;

namespace Skubbet.Entities.NonLivingEntities.Usable
{
    abstract class Weapon : UsableEntity
    {
        public int damage = 10;
        public List<Point> pattern = new List<Point>();
        private bool w = false;
        /// <summary>
        /// A weapon is an entity that can do damage to other entities
        /// </summary>
        /// <param name="x">the x position of the entity</param>
        /// <param name="y">the y position of the entity</param>
        /// <param name="tileSize">the size of a single tile</param>
        /// <param name="lvl">the level the entity exists in</param>
        public Weapon(int x, int y, int tileSize, Level lvl) : base(x, y, tileSize, lvl)
        {
            description = "This weapon does not have a description";
            name = "This weapon does not have a name";
        }
        /// <summary>
        /// Checks the weapons attack pattern up
        /// </summary>
        /// <param name="e">the <see cref="LivingEntity"/> that uses the weapon</param>
        public void AttackUp(LivingEntity e)
        {
            foreach (Point p in pattern)
            {
                if (lvl.GetRoomAt(e.x + p.X, e.y + p.Y) != null && !lvl.GetRoomAt(e.x + p.X, e.y + p.Y).getTileAt(e.x + p.X, e.y + p.Y).col)
                {
                    if (w && lvl.GetRoomAt(e.x + p.X, e.y + p.Y) != lvl.GetRoomAt(e.x, e.y))
                    {
                    }
                    else
                    {
                        w = false;
                        lvl.AddOverlay(new Tile(e.x + p.X, e.y + p.Y, new SolidBrush(Color.FromArgb(100, Color.Black))));
                        //lvl.GetRoomAt(e.x, e.y).InsertTile(new Point(e.x + p.X, e.y + p.Y), @"Images/Error.png", true);
                        if (lvl.GetEntityAt(e.x + p.X, e.y + p.Y) != null)
                        {
                            lvl.GetEntityAt(e.x + p.X, e.y + p.Y).TakeDamage(this);

                        }
                        if (OverWorldGameState.p.x == e.x + p.X && OverWorldGameState.p.y == e.y + p.Y)
                        {
                            OverWorldGameState.p.TakeDamage(this);
                        }
                    }

                }
                else
                {
                    w = true;
                }
            }
            w = false;
        }
        /// <summary>
        /// Checks the weapons attack pattern down
        /// </summary>
        /// <param name="e">the <see cref="LivingEntity"/> that uses the weapon</param>
        public void AttackDown(LivingEntity e)
        {
            foreach (Point p in pattern)
            {
                if (lvl.GetRoomAt(e.x + p.X, e.y + -p.Y) != null && !lvl.GetRoomAt(e.x + p.X, e.y + -p.Y).getTileAt(e.x + p.X, e.y + -p.Y).col)
                {
                    if (w && lvl.GetRoomAt(e.x + p.X, e.y + -p.Y) != lvl.GetRoomAt(e.x, e.y))
                    {
                    }
                    else
                    {
                        w = false;
                        lvl.AddOverlay(new Tile(e.x + p.X, e.y + -p.Y, new SolidBrush(Color.FromArgb(100, Color.Black))));
                        if (lvl.GetEntityAt(e.x + p.X, e.y + -p.Y) != null)
                        {

                            lvl.GetEntityAt(e.x + p.X, e.y + -p.Y).TakeDamage(this);
                        }
                        if (OverWorldGameState.p.x == e.x + p.X && OverWorldGameState.p.y == e.y + -p.Y)
                        {
                            OverWorldGameState.p.TakeDamage(this);
                        }
                    }
                }
                else
                {
                    w = true;
                }
            }
            w = false;
        }
        /// <summary>
        /// Checks the weapons attack pattern left
        /// </summary>
        /// <param name="e">the <see cref="LivingEntity"/> that uses the weapon</param>
        public void AttackLeft(LivingEntity e)
        {
            foreach (Point p in pattern)
            {
                if (lvl.GetRoomAt(e.x + p.Y, e.y + p.X) != null && !lvl.GetRoomAt(e.x + p.Y, e.y + p.X).getTileAt(e.x + p.Y, e.y + p.X).col)
                {
                    if (w && lvl.GetRoomAt(e.x + p.Y, e.y + p.X) != lvl.GetRoomAt(e.x, e.y))
                    {
                    }
                    else
                    {
                        w = false;
                        lvl.AddOverlay(new Tile(e.x + p.Y, e.y + p.X, new SolidBrush(Color.FromArgb(100, Color.Black))));
                        if (lvl.GetEntityAt(e.x + p.Y, e.y + p.X) != null)
                        {

                            lvl.GetEntityAt(e.x + p.Y, e.y + p.X).TakeDamage(this);
                        }
                        if (OverWorldGameState.p.x == e.x + p.Y && OverWorldGameState.p.y == e.y + p.X)
                        {
                            OverWorldGameState.p.TakeDamage(this);
                        }
                    }
                }
                else
                {
                    w = true;
                }
            }
            w = false;
        }
        /// <summary>
        /// Checks the weapons attack pattern right
        /// </summary>
        /// <param name="e">the <see cref="LivingEntity"/> that uses the weapon</param>
        public void AttackRight(LivingEntity e)
        {
            foreach (Point p in pattern)
            {
                if (lvl.GetRoomAt(e.x + -p.Y, e.y + p.X) != null && !lvl.GetRoomAt(e.x + -p.Y, e.y + p.X).getTileAt(e.x + -p.Y, e.y + p.X).col)
                {
                    if (w && lvl.GetRoomAt(e.x + -p.Y, e.y + p.X) != lvl.GetRoomAt(e.x, e.y))
                    {
                    }
                    else
                    {
                        w = false;
                        lvl.AddOverlay(new Tile(e.x + -p.Y, e.y + p.X, new SolidBrush(Color.FromArgb(100, Color.Black))));
                        if (lvl.GetEntityAt(e.x + -p.Y, e.y + p.X) != null)
                        {

                            lvl.GetEntityAt(e.x + -p.Y, e.y + p.X).TakeDamage(this);
                        }
                        if (OverWorldGameState.p.x == e.x + -p.Y && OverWorldGameState.p.y == e.y + p.X)
                        {
                            OverWorldGameState.p.TakeDamage(this);
                        }
                    }
                }
                else
                {
                    w = true;
                }
            }
            w = false;
        }
        /// <summary>
        /// Is called when it is picked up by a <see cref="LivingEntity"/>. a weapon is added to the <see cref="LivingEntity"/>s hand
        /// </summary>
        /// <param name="e">the <see cref="LivingEntity"/> that picks it up</param>
        public override void Pickup(LivingEntity e)
        {
            if(e.weapon != null)
            {
                e.weapon.Drop(e.x, e.y);
            }
            e.AddWeapon(this);
            base.Pickup(e);
        }
    }
}
