﻿using Skubbet.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Skubbet.Game
{
    class GameOverGameState : GameState
    {
        public GameOverGameState(Window w) : base(w)
        {
        }

        public override void Render(Graphics g)
        {
            g.DrawString("GAME OVER ", new Font(FontFamily.GenericSansSerif, 50), Brushes.Black, 100, 100);
        }

        public override void Tick(float delta)
        {
        }
    }
}
