﻿using Skubbet.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Skubbet.World;
using Skubbet.Entities.LivingEntities;
using System.Windows.Forms;

namespace Skubbet.Game
{
    class OverWorldGameState : GameState
    {
        private OverWorld ow;
        public static Player p;
        public Label currentWeapon = new Label();
        private Window w;
        public OverWorldGameState(Window w) : base(w)
        {
            this.w = w;
            p = new Player(5, 5, 20, ow);
            ow = new OverWorld(20, 0, 0, p);
            p.SetLevel(ow);
            currentWeapon.AutoSize = true;
            currentWeapon.Location = new Point(0, w.ClientRectangle.Height - 15);
            w.Controls.Add(currentWeapon);
            w.Refresh();
            w.ResizeEnd += new EventHandler(RefreshUI);
        }
        /// <summary>
        /// Refreshes the UI whenever called
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshUI(object sender, EventArgs e) {
            currentWeapon.Location = new Point(0, w.ClientRectangle.Height - 15);
            w.Refresh();
        }

        public override void Render(Graphics g)
        {
            ow.Render(g);
            p.Render(g);
        }

        public override void Tick(float delta)
        {
            if (p.weapon != null && currentWeapon.Text != p.weapon.name) {
                currentWeapon.Text = p.weapon.name;
                w.Refresh();
            }else if (p.weapon == null && currentWeapon.Text != "No weapon") {
                currentWeapon.Text = "No weapon";
                w.Refresh();
            }
            p.Behavior();
            if(p.health <= 0)
            {
                w.EnterGameState(new GameOverGameState(w));
            }
            ow.Tick(delta);
        }
    }
}
