﻿using Skubbet.Abstracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Skubbet {
    public partial class Window : Form {
        static List<Keys> pressedKeys = new List<Keys>();
        static List<Keys> releasedKeys = new List<Keys>();
        private GameState gs;
        public static Debug debug = new Debug();
        private int fps = 0;
        private long lastTickFps = 0;
        private long lastTickTime = 0;
        private int frames = 0;
        public static Random rnd = new Random();

        /// <summary>
        /// Initializes the window, and sets some important handlers to aid in game development (like keyUp and keyDown, and a timer to keep track of tick)
        /// </summary>
        public Window() {
            InitializeComponent();
            lastTickFps = DateTime.Now.Ticks;
            lastTickTime = DateTime.Now.Ticks;
            this.DoubleBuffered = true;
            
            this.Paint += new PaintEventHandler(Render);

            this.KeyDown += new KeyEventHandler(KeyDownEvent);
            this.KeyUp += new KeyEventHandler(KeyUpEvent);


        }
        /// <summary>
        /// Is an event that triggers when a given key is pressed down. 
        /// It adds the pressed keys to the pressedKeys List if the key is not pressed in the first place.
        /// </summary>
        /// <param name="sender">The object that sends the event.</param>
        /// <param name="e">Gives data about the pressed key that triggered the event.</param>
        private void KeyDownEvent(object sender, KeyEventArgs e) {
            if (!pressedKeys.Contains(e.KeyCode)) {
                pressedKeys.Add(e.KeyCode);
            }
        }
        /// <summary>
        /// Is an event that triggers when a given key is released. 
        /// It adds the pressed keys to the releasedKeys List, which gets cleared every tick. It then removes the key from the pressedKeys List
        /// </summary>
        /// <param name="sender">The object that sends the event.</param>
        /// <param name="e">Gives data about the released key that triggered the event.</param>
        private void KeyUpEvent(object sender, KeyEventArgs e) {
            releasedKeys.Add(e.KeyCode);
            pressedKeys.Remove(e.KeyCode);
        }

        /// <summary>
        /// Finds out if a given key is pressed down.
        /// </summary>
        /// <param name="key">The key that is checked</param>
        /// <returns>true or false depending on of the key is pressed</returns>
        public static bool IsKeyPressed(Keys key) {
            return pressedKeys.Where(x => x == key).Any() ? true : false;
        }
        /// <summary>
        /// Finds out if a given key is released.
        /// </summary>
        /// <param name="key">The key that is checked.</param>
        /// <returns>true or false depending on of the key is released.</returns>
        public static bool IsKeyReleased(Keys key) {
            return releasedKeys.Where(x => x == key).Any() ? true : false;
        }
        public static bool AnyKeyReleased()
        {
            return releasedKeys.Any() ? true : false;
        }

        /// <summary>
        /// Handles all the rendering. Is triggered everytime the window tries to "Paint".
        /// </summary>
        /// <param name="sender">The object that sends the event.</param>
        /// <param name="e">Gives data about the paint event (Contains "Grapgics").</param>
        private void Render(object sender, PaintEventArgs e) {
            Tick();
            if (gs != null) {
                gs.Render(e.Graphics);
            }
            Invalidate();
        }

        /// <summary>
        /// Enters a game state.
        /// </summary>
        /// <param name="gs">The game state to enter</param>
        public void EnterGameState(GameState gs) {
            this.gs = gs;


            this.Invalidate();
        }

        /// <summary>
        /// This is the "update" function of the game. It makes sure that the games time is not dependant on performance, but on time, by being tied to a timer.
        /// This is where you put all your calculations.
        /// </summary>
        /// <param name="sender">The object that sends the event.</param>
        /// <param name="e"></param>
        private void Tick() {
            int deltaTimeMs = (int)((DateTime.Now.Ticks - lastTickTime) / 10000f);
            lastTickTime = DateTime.Now.Ticks;
            long currentTime = DateTime.Now.Ticks;
            if (currentTime - lastTickFps > 10000000) {
                lastTickFps = currentTime;
                fps = frames;
                frames = 0;
                this.Text = "FPS: " + fps;
            }
            frames++;
            if(gs != null) {
                gs.Tick(deltaTimeMs);
            }
            if (IsKeyReleased(System.Windows.Forms.Keys.F5)&& !debug.Visible) {
                debug.Show(this);
                this.Focus();

            } else if(IsKeyReleased(System.Windows.Forms.Keys.F5)){
                debug.Hide();
            }
            releasedKeys.Clear();
        }

    }
}
