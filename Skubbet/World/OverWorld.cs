﻿using Skubbet.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Skubbet.Entities.NonLivingEntities;
using Skubbet.Entities.NonLivingEntities.Usable;
using Skubbet.Entities.LivingEntities;

namespace Skubbet.World
{
    class OverWorld : Level
    {

        private Room terrain;
        private Room testRoom;
        private Room greatHallW;
        private Room greatHallE;
        private Room citadel;
        private Room aqueduct;
        private BoxingGlove nl;
        private Gun1911 gun;
        private Bomb bomb;
        private Bow bow;
        private Branch branch;
        private DualHammers DHammer;
        private GlowingStick GlowStick;
        private Shotgun Sgun;
        private SniperRifle SRifle;
        private HeavyHammer HHamer;

        private RandomEnemy enemy;
        private AggressiveEnemy aggressiveEnemy;
        private SniperEnemy sniperEnemy;
        public OverWorld(int tileSize, int x, int y, Player p) : base(tileSize, x, y, p)
        {
            //Laver hele OverWorld, hvor spiller må gå i.
            terrain = new Room(30, 30, 1, 1,
                new List<Image> { Image.FromFile(@"Images\FloorTileWhite.png"), Image.FromFile(@"Images\FloorTile2.png") },
                new List<Image> { Image.FromFile(@"Images\WorldEdgeRight.png") },
                new List<Image> { Image.FromFile(@"Images\WorldEdgeCorner.png") },
                tileSize);
            rooms.Add(terrain);

            //Tilføjer bygning til OverWorld
            testRoom = new Room(5, 5, 10, 10,
                new List<Image> { Image.FromFile(@"Images\BuildingFloorRed.png"), Image.FromFile(@"Images\BuildingFloorGreen.png") },
                new List<Image> { Image.FromFile(@"Images\Building.png") },
                new List<Image> { Image.FromFile(@"Images\BuildingCorner.png") },
                tileSize);
            rooms.Add(testRoom);

            testRoom.InsertTile(new Point(3, 5), @"Images\DoorDown.png", false);
            testRoom.InsertTile(new Point(5, 3), @"Images\DoorRight.png", false);
            testRoom.InsertTile(new Point(1, 3), @"Images\DoorLeft.png", false);
            testRoom.InsertTile(new Point(3, 1), @"Images\DoorUpper.png", false);

            greatHallW = new Room(9, 4, 13, 3,
                new List<Image> { Image.FromFile(@"Images\BuildingFloorRed.png"), Image.FromFile(@"Images\BuildingFloorGreen.png") },
                new List<Image> { Image.FromFile(@"Images\Building.png") },
                new List<Image> { Image.FromFile(@"Images\BuildingCorner.png") },
                tileSize);
            rooms.Add(greatHallW);

            greatHallW.InsertTile(new Point(1, 2), @"Images\DoorLeft.png", false);
            greatHallW.InsertTile(new Point(1, 3), @"Images\DoorLeft.png", false);


            greatHallE = new Room(5, 10, 22, 3,
                new List<Image> { Image.FromFile(@"Images\BuildingFloorRed.png"), Image.FromFile(@"Images\BuildingFloorGreen.png") },
                new List<Image> { Image.FromFile(@"Images\Building.png") },
                new List<Image> { Image.FromFile(@"Images\BuildingCorner.png") },
                tileSize);
            rooms.Add(greatHallE);

            greatHallE.InsertTile(new Point(1, 2), @"Images\DoorLeft.png", false);
            greatHallE.InsertTile(new Point(1, 3), @"Images\DoorLeft.png", false);
            greatHallE.InsertTile(new Point(1, 6), @"Images\DoorLeft.png", false);
            greatHallE.InsertTile(new Point(3, 10), @"Images\DoorDown.png", false);





            ////Enemies and items

            nl = new BoxingGlove(2, 4, tileSize, this);
            nonLivings.Add(nl);

            gun = new Gun1911(10, 3, tileSize, this);
            nonLivings.Add(gun);


            bomb = new Bomb(13, 13, tileSize, this);

            bomb = new Bomb(10, 15, tileSize, this);

            nonLivings.Add(bomb);

            bow = new Bow(16, 13, tileSize, this);
            nonLivings.Add(bow);

            branch = new Branch(7, 4, tileSize, this);
            nonLivings.Add(branch);

            DHammer = new DualHammers(6, 15, tileSize, this);
            nonLivings.Add(DHammer);

            GlowStick = new GlowingStick(3, 16, tileSize, this);
            nonLivings.Add(GlowStick);

            HHamer = new HeavyHammer(12, 7, tileSize, this);
            nonLivings.Add(HHamer);


            Sgun = new Shotgun(28, 11, tileSize, this);
            nonLivings.Add(Sgun);

            SRifle = new SniperRifle(11, 25, tileSize, this);

            Sgun = new Shotgun(2, 18, tileSize, this);
            nonLivings.Add(Sgun);

            SRifle = new SniperRifle(12, 9, tileSize, this);

            nonLivings.Add(SRifle);

            //enemy = new RandomEnemy(5, 10, tileSize, this);
            //npcs.Add(enemy);

            //enemy = new RandomEnemy(25, 25, tileSize, this);
            //npcs.Add(enemy);
            enemy = new RandomEnemy(28, 28, tileSize, this);
            npcs.Add(enemy);
            enemy = new RandomEnemy(23, 23, tileSize, this);
            npcs.Add(enemy);
            enemy = new RandomEnemy(16, 10, tileSize, this);
            npcs.Add(enemy);
            enemy = new RandomEnemy(12, 20, tileSize, this);
            npcs.Add(enemy);

            aggressiveEnemy = new AggressiveEnemy(4, 12, tileSize, this);
            npcs.Add(aggressiveEnemy);

            aggressiveEnemy = new AggressiveEnemy(23, 11, tileSize, this);
            npcs.Add(aggressiveEnemy);
            aggressiveEnemy = new AggressiveEnemy(24, 11, tileSize, this);
            npcs.Add(aggressiveEnemy);
            //aggressiveEnemy = new AggressiveEnemy(25, 11, tileSize, this);
            //npcs.Add(aggressiveEnemy);
            aggressiveEnemy = new AggressiveEnemy(23, 9, tileSize, this);
            npcs.Add(aggressiveEnemy);
            //aggressiveEnemy = new AggressiveEnemy(24, 8, tileSize, this);
            //npcs.Add(aggressiveEnemy);
            aggressiveEnemy = new AggressiveEnemy(22, 2, tileSize, this);
            npcs.Add(aggressiveEnemy);

            //aggressiveEnemy = new AggressiveEnemy(18, 29, tileSize, this);
            //npcs.Add(aggressiveEnemy);
            aggressiveEnemy = new AggressiveEnemy(15, 29, tileSize, this);
            npcs.Add(aggressiveEnemy);
            //aggressiveEnemy = new AggressiveEnemy(18, 27, tileSize, this);
            //npcs.Add(aggressiveEnemy);
            aggressiveEnemy = new AggressiveEnemy(17, 27, tileSize, this);
            npcs.Add(aggressiveEnemy);
            aggressiveEnemy = new AggressiveEnemy(16, 27, tileSize, this);
            //npcs.Add(aggressiveEnemy);
            //aggressiveEnemy = new AggressiveEnemy(15, 27, tileSize, this);
            //npcs.Add(aggressiveEnemy);
            aggressiveEnemy = new AggressiveEnemy(14, 27, tileSize, this);
            npcs.Add(aggressiveEnemy);
            sniperEnemy = new SniperEnemy(5, 19, tileSize, this);
            npcs.Add(sniperEnemy);
            sniperEnemy = new SniperEnemy(20, 4, tileSize, this);
            npcs.Add(sniperEnemy);
            sniperEnemy = new SniperEnemy(21, 8, tileSize, this);
            npcs.Add(sniperEnemy);
            sniperEnemy = new SniperEnemy(12, 12, tileSize, this);
            npcs.Add(sniperEnemy);
            sniperEnemy = new SniperEnemy(19, 19, tileSize, this);
            npcs.Add(sniperEnemy);
        }
    }
}
