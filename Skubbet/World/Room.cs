﻿using Skubbet.Abstracts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skubbet.World
{
    class Room : GameObject
    {
        private List<List<Tile>> tileMap = new List<List<Tile>>();

        private int width = 0;
        private int height = 0;
        public int y = 0;
        public int x = 0;
        public Rectangle col;
        private List<Image> LeftWallImages = new List<Image>();
        private List<Image> RightWallImages = new List<Image>();
        private List<Image> UpWallImages = new List<Image>();
        private List<Image> DownWallImages = new List<Image>();
        private List<Image> TopLeftCornerImages = new List<Image>();
        private List<Image> TopRightCornerImages = new List<Image>();
        private List<Image> BottomLeftCornerImages = new List<Image>();
        private List<Image> BottomRightCornerImages = new List<Image>();
        private List<Image> floorImages = new List<Image>();


        /// <summary>
        /// A room consisting of tiles
        /// </summary>
        /// <param name="width">the width of the room in tiles</param>
        /// <param name="height">the height of the room in tiles</param>
        /// <param name="x">the horizontal position of the room in the world</param>
        /// <param name="y">the vertical position of the room in the world</param>
        /// <param name="tileSize">the size of a tile, in pixels</param>
        public Room(int width, int height, int x, int y, List<Image> floorImages, List<Image> wallImages, List<Image> cornerImages, int tileSize) : base(tileSize)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.floorImages = floorImages;
            col = new Rectangle(this.x, this.y, width * tileSize, height * tileSize);



            foreach (Image i in cornerImages)
            {
                TopRightCornerImages.Add((Image)i.Clone());
                i.RotateFlip(RotateFlipType.Rotate90FlipNone);
                BottomRightCornerImages.Add((Image)i.Clone());
                i.RotateFlip(RotateFlipType.Rotate90FlipNone);
                BottomLeftCornerImages.Add((Image)i.Clone());
                i.RotateFlip(RotateFlipType.Rotate90FlipNone);
                TopLeftCornerImages.Add((Image)i.Clone());
            }
            foreach (Image i in wallImages)
            {
                RightWallImages.Add((Image)i.Clone());
                i.RotateFlip(RotateFlipType.Rotate90FlipNone);
                DownWallImages.Add((Image)i.Clone());
                i.RotateFlip(RotateFlipType.Rotate90FlipNone);
                LeftWallImages.Add((Image)i.Clone());
                i.RotateFlip(RotateFlipType.Rotate90FlipNone);
                UpWallImages.Add((Image)i.Clone());
            }



            for (int ly = 0; ly < height; ly++)
            {
                List<Tile> tempMap = new List<Tile>();
                if (ly == 0)
                {
                    for (int lx = 0; lx < width; lx++)
                    {
                        if (lx == 0)
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, TopLeftCornerImages[Window.rnd.Next(0, TopLeftCornerImages.Count)], true)); //mure
                        }
                        else if (lx == width - 1)
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, TopRightCornerImages[Window.rnd.Next(0, TopRightCornerImages.Count)], true)); //mure
                        }
                        else
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, UpWallImages[Window.rnd.Next(0, UpWallImages.Count)], true)); //mure
                        }
                    }

                }
                else if (ly == height - 1)
                {
                    for (int lx = 0; lx < width; lx++)
                    {
                        if (lx == 0)
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, BottomLeftCornerImages[Window.rnd.Next(0, BottomLeftCornerImages.Count)], true)); //mure
                        }
                        else if (lx == width - 1)
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, BottomRightCornerImages[Window.rnd.Next(0, BottomRightCornerImages.Count)], true)); //mure
                        }
                        else
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, DownWallImages[Window.rnd.Next(0, DownWallImages.Count)], true)); //mure
                        }
                    }
                }
                else
                {
                    for (int lx = 0; lx < width; lx++)
                    {
                        if (lx == 0)
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, LeftWallImages[Window.rnd.Next(0, LeftWallImages.Count)], true)); //mure
                        }
                        else if (lx == width - 1)
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, RightWallImages[Window.rnd.Next(0, RightWallImages.Count)], true)); //mure
                        }
                        else
                        {
                            tempMap.Add(new Tile((this.x + lx) * tileSize, (this.y + ly) * tileSize, floorImages[Window.rnd.Next(0, floorImages.Count)], false)); //baggrund
                        }
                    }
                }

                tileMap.Add(tempMap);
            }
        }
        /// <summary>
        /// Renders the collisionShape of the room
        /// </summary>
        /// <param name="g">GDI drawing surface instance</param>
        /// <param name="color">what color to render the shape in</param>
        public void RenderCol(Graphics g, Pen color)
        {
            g.DrawRectangle(color, col);


        }
        /// <summary>
        /// Renders the room when player is not in it
        /// </summary>
        /// <param name="g">The GDI control element used to draw</param>
        /// <param name="spriteDest">the destination to the tile that should be rendered</param>
        public void RenderUnoccupied(Graphics g, string spriteDest)
        {
            Image i = Image.FromFile(spriteDest);
            foreach (List<Tile> tl in tileMap)
            {
                if (tl == tileMap[0] || tl == tileMap[tileMap.Count - 1])
                {
                    foreach (Tile t in tl)
                    {
                        g.DrawImage(t.image, t.x, t.y, tileSize, tileSize);
                    }
                }
                else
                {
                    foreach (Tile t in tl)
                    {
                        if (t == tl[0] || t == tl[tl.Count - 1])
                        {
                            g.DrawImage(t.image, t.x, t.y, tileSize, tileSize);
                        }
                        else
                        {
                            g.DrawImage(t.image, t.x, t.y, tileSize, tileSize);
                            g.FillRectangle(new SolidBrush(Color.FromArgb(100, 0, 0, 0)), t.x, t.y, tileSize, tileSize);

                        }
                    }
                }

            }
        }

        public override void Render(Graphics g)
        {
            foreach (List<Tile> tl in tileMap)
            {
                foreach (Tile t in tl)
                {
                    if (t.image != null)
                    {
                        g.DrawImage(t.image, t.x, t.y, tileSize, tileSize);
                    }
                    else
                    {
                        g.FillRectangle(t.brush, t.x, t.y, tileSize, tileSize);
                    }
                }
            }

        }

        /// <summary>
        /// Gets all rooms that collide with a given room
        /// </summary>
        /// <param name="r">the room to check against</param>
        /// <returns>A list of the colliding rooms</returns>
        public static List<Room> GetAllCollidingRooms(List<Room> r)
        {
            List<Room> rr = new List<Room>();

            foreach (Room room in r)
            {
                foreach (Room room2 in r)
                {

                    if (room2.col.IntersectsWith(room.col))
                    {
                        if (room2 != room)
                        {
                            rr.Add(room);
                        }
                    }
                }
            }

            return rr;
        }

        /// <summary>
        /// Gets all rooms from a list that collides with this room.
        /// </summary>
        /// <param name="r">the list of rooms to check</param>
        /// <returns>a list of all the rooms that collide</returns>
        public List<Room> GetCollidingRooms(List<Room> r)
        {
            List<Room> rr = new List<Room>();
            foreach (Room room in r)
            {
                Rectangle r2 = new Rectangle(room.x, room.y, room.GetWidth(), room.GetHeight());
                if (col.IntersectsWith(room.col))
                {
                    if (room != this)
                    {
                        rr.Insert(0, room);
                    }
                }
            }
            return rr;
        }

        public void SetPosX(int x)
        {
            this.x = x;
            col.X = x * tileSize;
        }
        public void SetPosY(int y)
        {
            this.y = y;
            col.Y = y * tileSize;
        }

        public int GetHeight()
        {
            return height;
        }
        public int GetWidth()
        {
            return width;
        }
        /// <summary>
        /// Inserts a tile at a point
        /// </summary>
        /// <param name="p">the point</param>
        /// <param name="img">the image</param>
        /// <param name="col">does it collide?</param>
        public void InsertTile(Point p, string img, bool col)
        {
            tileMap[p.Y - 1][p.X - 1] = new Tile((p.X - 1 + this.x) * tileSize, (p.Y - 1 + this.y) * tileSize, Image.FromFile(img), col);
        }
        /// <summary>
        /// Inserts a tile at a point
        /// </summary>
        /// <param name="x">x of the point</param>
        /// <param name="t">y of the point</param>
        /// <param name="img">the image</param>
        /// <param name="col">does it collide?</param>
        public void InsertTile(int x, int y, string img, bool col)
        {
            tileMap[y][x] = new Tile(y, x, Image.FromFile(img), col);
        }
        /// <summary>
        /// returns the tile at a given position
        /// </summary>
        /// <param name="x">the x to check</param>
        /// <param name="y">the y to check</param>
        /// <returns></returns>
        public Tile getTileAt(int x, int y)
        {
            return tileMap[(y - this.y)][x - this.x];
        }

        public override void Tick(float delta)
        {

        }
    }
}
