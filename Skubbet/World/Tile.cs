﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skubbet.World {
    class Tile {
        public int x = 0;
        public int y = 0;
        public bool col = false;
        public Brush brush { get; set; }
        public Image image;
        public Tile(int x, int y, Brush c) {
            this.x = x;
            this.y = y;
            this.brush = c;
        }
        public Tile(int x, int y) {
            this.x = x;
            this.y = y;
        }
        public Tile(int x, int y, Image img, bool col)
        {
            this.x = x;
            this.y = y;
            this.image = img;
            this.col = col;
        }
    }
}
